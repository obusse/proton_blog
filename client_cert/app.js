// domino-db
const {
    useServer
} = require('@domino/domino-db');

// read client certificates
const fs = require('fs');
const path = require('path');

const readFile = fileName => {
    try {
        return fs.readFileSync(path.resolve(fileName));
    } catch (error) {
        return undefined;
    }
};

const rootCertificate = readFile('./certs/ca.crt');
const clientCertificate = readFile('./certs/app1.crt');
const clientKey = readFile('./certs/app1.key');

// proton config
const serverConfig = {
    "hostName": "domino10.local",
    "connection": {
        "port": 3002
    }
}

function init() {
    useServer(serverConfig).then(async server => {
        console.log(server);
    }).catch(error => {
        console.log("%s. Cause: %s", error.message, error.code);
    });
}

// start the app
init();